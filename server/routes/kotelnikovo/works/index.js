const router = require(`express`).Router();
const { works: worksController } = require(`../../../controllers/kotelnikovo/index.controller`);

router.post(`/addChildWork`, worksController.uploadChildWork);
router.post(`/addArchitectWork`, worksController.uploadArchitectWork);
router.get(`/getWorksCount`, worksController.getWorksCount);
router.get(`/getWorksPage`, worksController.getWorksPage);

module.exports = router;

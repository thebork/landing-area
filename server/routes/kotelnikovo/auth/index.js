const router = require(`express`).Router();

const authController = require(`../../../controllers/kotelnikovo/auth.controller`);

router.post(`/signin`, authController.authenticateJwt);
router.post(`/register`, authController.addAdmin);
router.get(`/getUser`, authController.checkJwtAuthentication, (req, res) => res.status(200).json({ ok: true }));

module.exports = router;

const router = require(`express`).Router();

const infoController = require(`../../../controllers/kotelnikovo/info.controller`);
const authController = require(`../../../controllers/kotelnikovo/auth.controller`);

router.get(`/`, infoController.get);
router.post(`/`, authController.checkJwtAuthentication, infoController.post);

module.exports = router;

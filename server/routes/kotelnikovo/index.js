const router = require(`express`).Router();

const voteRouter = require(`./vote`);
const filesRouter = require(`./files`);
const worksRouter = require(`./works`);
const authRouter = require(`./auth`);
const infoRouter = require(`./info`);

router.use(`/vote`, voteRouter);
router.use(`/files`, filesRouter);
router.use(`/info`, infoRouter);
router.use(`/`, worksRouter);
router.use(`/auth`, authRouter);

module.exports = router;

const router = require(`express`).Router();
const { vote: voteController } = require(`../../../controllers/kotelnikovo/index.controller`);

router.post(`/setEmail`, voteController.setEmail);
router.post(`/sendCode`, voteController.sendCode);

module.exports = router;

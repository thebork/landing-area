const router = require(`express`).Router();
const { files: filesController } = require(`../../../controllers/kotelnikovo/index.controller`);
const authController = require(`../../../controllers/kotelnikovo/auth.controller`);

router.get(`/photos`, filesController.getPhotos(`public/uploads/photos/`));
router.post(`/photos`, authController.checkJwtAuthentication, filesController.uploadFiles(`public/uploads/photos/`));
router.delete(`/photos`, authController.checkJwtAuthentication, filesController.removeFile(`public/uploads/photos/`));
router.get(`/images`, authController.checkJwtAuthentication, filesController.getPhotos(`public/uploads/images/`));
router.post(`/images`, authController.checkJwtAuthentication, filesController.uploadFiles(`public/uploads/images/`));
router.delete(`/images`, authController.checkJwtAuthentication, filesController.removeFile(`public/uploads/images/`));

module.exports = router;

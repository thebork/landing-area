const router = require('express').Router();

const emailRouter = require('./email');
const kotelnikovoRouter = require('./kotelnikovo');

router.use(`/`, emailRouter);
router.use(`/kotelnikovo`, kotelnikovoRouter);

module.exports = router;

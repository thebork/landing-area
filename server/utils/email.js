const mailer = require(`../config/mailer`);

module.exports = {
  sendCode: async ({ code, email }) => {
    try {
      const status = await mailer.sendMail({
        from: `Котельниково - Земля Героев <${process.env.EMAIL_USERNAME}>`,
        to: email,
        subject: `Код для голосования на сайте "Котельниково - Земля Героев"`,
        text: `Для того, чтобы проголосовать в конкурсе, введите на сайте следующий код: ${code}.`,
      });
      return status;
    } catch (e) {
      return e;
    }
  },
};

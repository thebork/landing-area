const _ = require(`lodash`);
const fs = require(`fs`);
const path = require(`path`);

module.exports = {
  isValidCompetition: (competition) => !(competition !== `child` && competition !== `architect`),
  isValidEmail: (email) => (/\S+@\S+\.\S+/).test(email),
  logWarning: ({ error, message }) => {
    const logMessage = error ? (_.get(error, `message`) || _.get(error, `response`) || message) : message;
    console.log(logMessage);
  },
  errorResponse: (res, {
    error,
    data = [],
    code = 400,
    defaultMessage = `Произошла ошибка`,
  }) => {
    const logMessage = error ? (_.get(error, `message`) || _.get(error, `response`) || defaultMessage) : defaultMessage;
    console.log(logMessage);
    return res.status(code)
      .json({
        ok: false,
        data,
        message: defaultMessage,
      });
  },
  checkFileLimits: ({ file, maxSize, availableTypes }) => {
    const fileSize = _.get(file, `size`);
    const fileType = _.get(file, `mimetype`);
    const fileName = _.get(file, `originalname`);

    if (!_.includes(availableTypes, fileType)) return `Недопустимое расширение файла ${fileName}`;
    if (fileSize > maxSize) return `Превышен максимальный размер файла ${fileName}`;

    return null;
  },
  uploadFile: ({ file, fileName, pathName = `public/uploads` }) => {
    let _pathName = path.join(pathName);
    if (_pathName.startsWith(`/`)) _pathName = _pathName.substr(1);
    if (!_pathName.endsWith(`/`)) _pathName += `/`;
    const buffer = _.get(file, `buffer`);
    const _fileName = fileName || `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`;
    const wstream = fs.createWriteStream(`./${_pathName}${_fileName}`);
    wstream.write(buffer);
    return _fileName;
  },

  removeNoImages: (array) => array.filter((x) => (x.search(/(jpg|jpeg|png|svg)$/gi) > -1)),
};

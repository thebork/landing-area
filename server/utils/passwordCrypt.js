const SimpleCrypto = require(`simple-crypto-js`).default;
const secretCryptoKey = process.env.SIMPLE_CRYPTO_SECRET;
const simpleCrypto = new SimpleCrypto(secretCryptoKey);

module.exports = {
  encrypt: (password) => simpleCrypto.encrypt(password),
  decrypt: (passwordHash) => simpleCrypto.decrypt(passwordHash),
};

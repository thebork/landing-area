const db = require(`../../config/kotelnikovo.db`);

module.exports = {
  getInfo: async (block) => {
    const [row] = await db.query(`SELECT * FROM information${block ? ` WHERE block='${block}'` : ``}`);
    return row;
  },
  setInfo: async ({ name, value }) => {
    const [row] = await db.query(`UPDATE information SET value=? WHERE slug=?`, [value, name]);
    return row;
  },
};

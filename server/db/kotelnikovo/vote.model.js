const db = require(`../../config/kotelnikovo.db`);

module.exports = {
  createCode: async ({
    email, code, createTimestamp,
  }) => {
    try {
      await db.query(
        `INSERT INTO codes(email, code, create_timestamp) VALUES (?, ?, ?)`, [email, code, createTimestamp]);
      return null;
    } catch (e) {
      return e;
    }
  },
  updateCode: async ({
    email, code, createTimestamp,
  }) => {
    try {
      await db.query(`UPDATE codes SET code=?, create_timestamp=?, attempts=0 WHERE email=?`,
        [code, createTimestamp, email]);
      return null;
    } catch (e) {
      return e;
    }
  },
  deleteCode: async ({ email }) => {
    try {
      await db.query(`DELETE FROM codes WHERE email=?`, [email]);
      return null;
    } catch (e) {
      return e;
    }
  },
  addVotesHistoryRow: async ({
    email, competition, workId, createTimestamp,
  }) => {
    try {
      await db.query(
        `INSERT INTO votes(email, competition, work_id, create_timestamp) VALUES (?, ?, ?, ?)`,
        [email, competition, workId, createTimestamp],
      );
      return null;
    } catch (e) {
      return e;
    }
  },
  getVotesHistoryRow: async ({ email, competition }) => {
    try {
      const [row] = await db.query(
        `SELECT * FROM votes WHERE email=? AND competition=?`,
        [email, competition],
      );
      return [null, row];
    } catch (e) {
      return [e];
    }
  },
  increaseChildWorkRating: async ({ id }) => {
    try {
      const [row] = await db.query(`UPDATE child_works SET votes=votes+1 WHERE id=?`, [id]);
      return [null, row];
    } catch (e) {
      return [e];
    }
  },
  increaseArchitectWorkRating: async ({ id }) => {
    try {
      const [row] = await db.query(`UPDATE architect_works SET votes=votes+1 WHERE id=?`, [id]);
      return [null, row];
    } catch (e) {
      return [e];
    }
  },
  getCode: async ({ email }) => {
    try {
      const [row] = await db.query(
        `SELECT * FROM codes WHERE email=?`,
        [email],
      );
      return [null, row];
    } catch (e) {
      return [e];
    }
  },
  increaseCodeSendingAttempt: async ({ email }) => {
    try {
      await db.query(
        `UPDATE codes SET attempts=attempts+1 WHERE email=?`,
        [email],
      );
      return null;
    } catch (e) {
      return e;
    }
  },
};

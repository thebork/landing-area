const db = require(`../../config/kotelnikovo.db`);

module.exports = {
  createUser: async ({ username, password, creationDate }) => {
    const [row] = await db.query(
      `INSERT INTO admins(login, password, create_date) VALUES(?,?,?)`, [username, password, creationDate],
    );
    return row;
  },
  getUserByLogin: async (login) => {
    const [row] = await db.query(`SELECT * FROM admins WHERE login=?`, [login]);
    return row;
  },
  setUserToken: async ({ login, token }) => {
    const [row] = await db.query(`UPDATE admins SET token=? WHERE login=?`, [token, login]);
    return row;
  },
};

const works = require(`./works.controller`);
const vote = require(`./vote.controller`);
const files = require(`./files.controller`);

module.exports = {
  works,
  vote,
  files,
};

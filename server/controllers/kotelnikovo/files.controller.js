const _ = require(`lodash`);
const fs = require(`fs`);
const {
  errorResponse,
  removeNoImages,
  checkFileLimits,
  uploadFile,
} = require(`../../utils/helper`);


const multer = require(`multer`);

const multerUploadPhoto = multer({
  storage: multer.memoryStorage(),
  limits: {
    fieldNameSize: 255,
    fileSize: 2 * 1024 * 1024, // 2Mb
  },
})
  .fields([{
    name: `files`,
    maxCount: 50,
  }]);

const getFile = (files, name) => _.first(_.get(files, name));

module.exports = {
  getPhotos: (pathName) => async (req, res) => {
    console.log(req.body);
    fs.readdir(pathName, (error, files) => {
      if (error) {
        return errorResponse(res, { error, code: 500, defaultMessage: `Ошибка чтения папки` });
      }
      if (!files || !_.size(files)) {
        return res.status(200).json({ ok: true, message: `В папке нет файлов`, data: [] });
      }
      const imagesArray = removeNoImages(files);
      res.status(200).json({ ok: true, message: `Файлы успешно получены`, data: imagesArray });
    });
  },
  uploadFiles: (pathName) => async (req, res) => {
    multerUploadPhoto(req, res, (error) => {
      if (error) return errorResponse(res, { error, defaultMessage: `Ошибка загрузки фотографии` });

      const files = _.get(req, `files.files`);

      if (!_.size(files)) return errorResponse(res, { defaultMessage: `Не найдено файлов` });

      const isLimitErrors = files.some((item) => {
        const fileErrors = checkFileLimits({
          file: item,
          maxSize: 2 * 1024 * 1024,
          availableTypes: [`image/jpeg`, `image/pjpeg`, `image/png`, `image/svg+xml`],
        });
        return !!fileErrors;
      });

      if (isLimitErrors) return errorResponse(res, { defaultMessage: `Один или несколько файлов некорректны` });

      const uploadedFiles = files.map((item) => uploadFile({ file: item, fileName: item.originalname, pathName }));

      res.status(200)
        .json({
          ok: true,
          message: `Upload successful`,
          data: {
            files: uploadedFiles,
          },
        });
    });
  },
  removeFile: (pathName) => async (req, res) => {
    const { file } = req.query;
    fs.readdir(pathName, (error, files) => {
      if (error) {
        return errorResponse(res, { error, code: 500, defaultMessage: `Ошибка чтения папки` });
      }
      if (!files || !_.size(files) || _.find(files, file) === -1) {
        return errorResponse(res, { error, code: 404, defaultMessage: `Не найден файл, который вы хотите удалить.` });
      }
      console.log(pathName + file);

      fs.unlinkSync(pathName + file);
      res.status(200).json({ ok: true, message: `Файл успешно удален` });
    });
  },
};

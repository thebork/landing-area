const _ = require(`lodash`);
const passport = require(`../../middlewares/passport`);
const passwordCrypt = require(`../../utils/passwordCrypt`);


const { errorResponse } = require(`../../utils/helper`);

const { getUserByLogin, setUserToken, createUser } = require(`../../db/kotelnikovo/admin.model`);

module.exports = {
  authenticateJwt: async (req, res) => {
    const { username: login, password } = req.body;
    if (!login || !password) return errorResponse(res, { code: 400, defaultMessage: `Не заполнены обязательные поля` });
    try {
      const userResponse = await getUserByLogin(login);
      if (!_.size(userResponse)) {
        return errorResponse(res, { code: 401, defaultMessage: `Пользователь с таким логином не найден` });
      }

      const _password = _.get(_.first(userResponse), `password`);
      if (password !== passwordCrypt.decrypt(_password)) {
        return errorResponse(res, { code: 401, defaultMessage: `Неправильный пароль` });
      }

      const token = passport.getToken({ login });
      const tokenResponse = await setUserToken({ login, token });
      const isUserUpdated = _.toFinite(_.get(tokenResponse, `affectedRows`)) === 1;
      if (!isUserUpdated) throw new Error(`Error in set token`);
      res.status(200).json({ ok: true, message: `Login successfully`, data: { token } });
    } catch (error) {
      errorResponse(res, { error, code: 400, defaultMessage: `Ошибка при авторизации` });
    }
  },
  checkJwtAuthentication: (req, res, next) => {
    passport.authenticate(`jwt`, { session: false }, (err, user) => {
      if (err) return errorResponse(res, { code: 400, defaultMessage: `Error in check authorization` });
      if (!user) return errorResponse(res, { code: 401, defaultMessage: `Unathorized` });
      const _user = _.first(user);
      const login = _.get(_user, `login`);
      req.user = { login };
      next();
    })(req, res, next);
  },
  addAdmin: async (req, res) => {
    const { username, password } = req.body;
    if (!username || !password) return errorResponse(res, { defaultMessage: `Required fields are missed` });
    try {
      await createUser({ username, password: passwordCrypt.encrypt(password), creationDate: +new Date() });
      return res.status(200).json({ ok: true, message: `Admin added` });
    } catch (error) {
      errorResponse(res, { error, defaultMessage: `Error in adding admin` });
    }
  },
};

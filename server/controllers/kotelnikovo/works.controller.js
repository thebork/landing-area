const {
  addChildWork,
  addArchitectWork,
  countChildWorks,
  countArchitectWorks,
  getChildWorks,
  getArchitectWorks,
} = require(`../../db/kotelnikovo/works.model`);

const {
  isValidCompetition,
  checkFileLimits,
  uploadFile,
  errorResponse,
} = require(`../../utils/helper`);
const _ = require(`lodash`);
const fs = require(`fs`);

const multer = require(`multer`);

const RESULTS_ON_PAGE = 10;

const uploadChild = multer({
  storage: multer.memoryStorage(),
  limits: {
    fieldNameSize: 255,
    fileSize: 1 * 1024 * 1024, // 1Mb
  },
})
  .fields([{
    name: `work`,
    maxCount: 1,
  }]);

const uploadArchitect = multer({
  storage: multer.memoryStorage(),
  limits: {
    fieldNameSize: 255,
    fileSize: 1 * 1024 * 1024, // 1Mb
  },
})
  .fields([{
    name: `photo`,
    maxCount: 1,
  }, {
    name: `work`,
    maxCount: 1,
  }]);

const getFile = (files, name) => _.first(_.get(files, name));

module.exports = {
  uploadChildWork: (req, res) => {
    uploadChild(req, res, (error) => {
      if (error) {
        errorResponse(res, { error, defaultMessage: `Ошибка загрузки файлов` });
        return;
      }

      const {
        name,
        age,
        studyPlace,
        aboutWork: workAbout,
      } = req.body;
      if (!name || !age || !studyPlace || !workAbout) {
        errorResponse(res, { defaultMessage: `Не заполнены обязательные поля` });
        return;
      }

      const workFile = getFile(_.get(req, `files`), `work`);

      if (!workFile) {
        errorResponse(res, { defaultMessage: `Отсутствует один или несколько файлов` });
        return;
      }

      const workFileErrors = checkFileLimits({
        file: workFile,
        maxSize: 1 * 1024 * 1024,
        availableTypes: [`application/pdf`],
      });

      if (workFileErrors) {
        errorResponse(res, { error: workFileErrors, defaultMessage: `Некорректный файл` });
        return;
      }

      const workFileName = uploadFile({ file: workFile });

      addChildWork({
        name,
        age,
        studyPlace,
        workAbout,
        work: workFileName,
      })
        .then(() => {
          res.status(200)
            .json({
              ok: true,
              message: `Upload successful`,
              data: {
                name,
                age,
                studyPlace,
                workAbout,
                files: [{ type: `work`, filename: workFileName }],
              },
            });
        })
        .catch((err) => {
          return errorResponse(res, { error: err, defaultMessage: `Ошибка во время сохранения работы` });
        });
    });
  },
  uploadArchitectWork: (req, res) => {
    uploadArchitect(req, res, (err) => {
      if (err) {
        errorResponse(res, { error: err, defaultMessage: `Ошибка загрузки файлов` });
        return;
      }

      const {
        name,
        age,
        studyPlace,
        aboutWork: workAbout,
      } = req.body;
      if (!name || !age || !studyPlace || !workAbout) {
        errorResponse(res, { defaultMessage: `Не заполнены обязательные поля` });
        return;
      }

      const photoFile = getFile(_.get(req, `files`), `photo`);
      const workFile = getFile(_.get(req, `files`), `work`);

      if (!photoFile || !workFile) {
        errorResponse(res, { defaultMessage: `Отсутствует один или несколько файлов` });
        return;
      }

      const photoFileErrors = checkFileLimits({
        file: photoFile,
        maxSize: 1 * 1024 * 1024,
        availableTypes: [`image/jpeg`, `image/pjpeg`, `image/png`],
      });

      if (photoFileErrors) {
        errorResponse(res, { error: photoFileErrors, defaultMessage: `Некорректный файл фотографии` });
        return;
      }

      const workFileErrors = checkFileLimits({
        file: workFile,
        maxSize: 1 * 1024 * 1024,
        availableTypes: [`application/pdf`],
      });

      if (workFileErrors) {
        errorResponse(res, { error: workFileErrors, defaultMessage: `Некорректный файл работы` });
        return;
      }

      const photoFileName = uploadFile({ file: photoFile });
      const workFileName = uploadFile({ file: workFile });

      addArchitectWork({
        name,
        age,
        studyPlace,
        workAbout,
        photo: photoFileName,
        work: workFileName,
      })
        .then(() => {
          res.status(200)
            .json({
              ok: true,
              message: `Upload successful`,
              data: {
                name,
                age,
                studyPlace,
                workAbout,
                files: [
                  { type: `photo`, filename: photoFileName },
                  { type: `work`, filename: workFileName },
                ],
              },
            });
        })
        .catch((error) => {
          errorResponse(res, { error, defaultMessage: `Ошибка во время сохранения работы` });
        });
    });
  },
  getWorksCount: async (req, res) => {
    const { competition } = req.query;
    try {
      if (!isValidCompetition(competition)) throw new Error(`Запрашиваемого конкурса не существует`);
      const getCount = competition === `child` ? countChildWorks : countArchitectWorks;
      const resultsAmount = await getCount();
      const pagesAmount = resultsAmount ? _.ceil(resultsAmount / RESULTS_ON_PAGE) : 1;
      res.status(200)
        .json({
          ok: true,
          message: `Success`,
          data: {
            resultsAmount,
            pagesAmount,
          },
        });
    } catch (e) {
      errorResponse(res, { error: e });
    }
  },
  getWorksPage: async (req, res) => {
    const { competition, page: currentPage } = req.query;

    const from = RESULTS_ON_PAGE * (currentPage - 1);
    try {
      if (!isValidCompetition(competition)) throw new Error(`Запрашиваемого конкурса не существует`);
      const getWorks = competition === `child` ? getChildWorks : getArchitectWorks;
      const response = await getWorks({ from, limit: RESULTS_ON_PAGE });
      res.status(200)
        .json({
          ok: true,
          message: `Success`,
          data: response,
        });
    } catch (e) {
      errorResponse(res, { error: e });
    }
  },
};

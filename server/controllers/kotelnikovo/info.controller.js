const _ = require(`lodash`);
const { errorResponse } = require(`../../utils/helper`);
const { getInfo, setInfo } = require(`../../db/kotelnikovo/info.model`);

const uploadFile = (file) => {
  const buffer = _.get(file, `buffer`);
  const fileName = `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`;
  const wstream = fs.createWriteStream(`./public/uploads/${fileName}`);
  wstream.write(buffer);
  return fileName;
};

module.exports = {
  get: async (req, res) => {
    const { block } = req.query;
    try {
      const response = await getInfo(block);
      res.status(200).json({ ok: true, message: `Данный получены`, data: response });
    } catch (error) {
      return errorResponse(res, { error, defaultMessage: `Ошибка запроса информации` });
    }
  },
  post: async (req, res) => {
    const { name, value } = req.body;
    if (!name || !value) return errorResponse(res, { defaultMessage: `Не зпполнены обязательные поля` });
    try {
      const response = await setInfo({ name, value });
      const isInfoUpdated = _.toFinite(_.get(response, `affectedRows`)) === 1;
      if (!isInfoUpdated) {
        return errorResponse(res, { defaultMessage: `Во время обновления информации произошла ошибка`})
      }
      res.status(200).json({ ok: true, message: `Информация успешно обновлена` });
    } catch (error) {
      return errorResponse(res, { error, defaultMessage: `Ошибка обновления информации` });
    }
  },
};

const _ = require(`lodash`);

const {
  createCode,
  updateCode,
  deleteCode,
  getCode,
  getVotesHistoryRow,
  addVotesHistoryRow,
  increaseChildWorkRating,
  increaseArchitectWorkRating,
  increaseCodeSendingAttempt,
} = require(`../../db/kotelnikovo/vote.model`);

const {
  isValidCompetition,
  isValidEmail,
  logWarning,
  errorResponse
} = require(`../../utils/helper`);

const { sendCode } = require(`../../utils/email`);

const MILLISECONDS_TO_CODE_EXPIRE = 30 * 60 * 1000;
const MILLISECONDS_TO_NEW_CODE = 1 * 60 * 1000;
const MAX_CODE_ATTEMPTS = 30;

module.exports = {
  setEmail: async (req, res) => {
    const { email, competition } = req.body;
    if (!email || !competition) {
      return errorResponse(res, { defaultMessage: `Не заполнены обязательные поля` });
    }

    if (!isValidEmail(email)) return errorResponse(res, { defaultMessage: `Некорректный email` });

    if (!isValidCompetition(competition)) {
      return errorResponse(res, { defaultMessage: `Не найдено конкурса с таким названием` });
    }

    try {
      const [getHistoryError, getHistoryResult] = await getVotesHistoryRow({ email, competition });

      if (getHistoryError) {
        return errorResponse(res, { error: getHistoryError, defaultMessage: `Ошибка проверки email` });
      }
      if (_.size(getHistoryResult)) return errorResponse(res, { defaultMessage: `Вы уже голосовали в этом конкурсе` });

      const [getCodeError, getCodeResult] = await getCode({ email });

      if (getCodeError) return errorResponse(res, { error: getCodeError, defaultMessage: `Отсутствует email` });

      const codeRow = _.first(getCodeResult);
      if (codeRow) {
        const timestamp = +new Date();
        const creationTimestamp = _.toFinite(_.get(codeRow, `create_timestamp`));
        const newCodeMinTimestamp = creationTimestamp + MILLISECONDS_TO_NEW_CODE;
        if (timestamp < newCodeMinTimestamp) {
          const secondsToNewCodeRequest = _.ceil((newCodeMinTimestamp - timestamp) / 1000);
          return errorResponse(res, {
            data: {
              secondsToNewCode: secondsToNewCodeRequest,
            },
            defaultMessage: `Код можно будет запросить через ${secondsToNewCodeRequest} секунд.`,
          });
        }
      }

      const code = Math.floor((Math.random() * (9999 - 1000)) + 1000);
      const timestamp = +new Date();

      const setCode = codeRow ? updateCode : createCode;
      const error = await setCode({
        email,
        code,
        createTimestamp: timestamp,
      });
      if (error) return errorResponse(res, { error, defaultMessage: `Ошибка создания кода` });

      const sendStatus = await sendCode({ code, email });
      const messageId = _.get(sendStatus, `messageId`);
      if (!messageId) return errorResponse(res, { error: sendStatus, defaultMessage: `Ошибка отправки кода` });

      res.status(200)
        .json({
          ok: true,
          message: `Создан код для ${email}`,
          data: {
            email,
            creationAt: timestamp,
            expiresIn: timestamp + MILLISECONDS_TO_CODE_EXPIRE,
            newCodeIn: timestamp + MILLISECONDS_TO_NEW_CODE,
          },
        });
    } catch (e) {
      errorResponse(res, { error: e, defaultMessage: `Ошибка во время обработки запроса` });
    }
  },
  sendCode: async (req, res) => {
    const {
      email, code, competition, id,
    } = req.body;

    if (!email || !code || !competition || !id) {
      return errorResponse(res, { defaultMessage: `Не заполнены обязательные поля` });
    }

    if (!isValidEmail(email)) return errorResponse(res, { defaultMessage: `Некорректный email` });

    if (!isValidCompetition(competition)) {
      return errorResponse(res, { defaultMessage: `Не найдено конкурса с таким названием` });
    }

    try {
      const [getHistoryError, getHistoryResult] = await getVotesHistoryRow({ email, competition });

      if (getHistoryError) {
        return errorResponse(res, { error: getHistoryError, defaultMessage: `Ошибка проверки email` });
      }
      if (_.size(getHistoryResult)) return errorResponse(res, { defaultMessage: `Вы уже голосовали в этом конкурсе` });

      const [getCodeError, getCodeResult] = await getCode({ email });
      if (getCodeError) return errorResponse(res, { error: getCodeError, defaultMessage: `Ошибка запроса кода` });

      const codeRow = _.first(getCodeResult);
      if (!codeRow) return errorResponse(res, { defaultMessage: `Не найден код для этого email` });

      const timestamp = +new Date();
      const creationTimestamp = _.toFinite(_.get(codeRow, `create_timestamp`));
      if (timestamp > creationTimestamp + MILLISECONDS_TO_CODE_EXPIRE) {
        const deleteCodeError = await deleteCode({ email });
        if (deleteCodeError) logWarning({ error: deleteCodeError, message: `Ошибка удаления кода` });
        return errorResponse(res, { defaultMessage: `Истек срок действия кода` });
      }

      const previousAttempts = _.get(codeRow, `attempts`);
      if (typeof previousAttempts !== `number`) {
        return errorResponse(res, { defaultMessage: `Ошибка запроса параметров кода` });
      }

      const attempts = previousAttempts + 1;

      if (attempts > MAX_CODE_ATTEMPTS) {
        const deleteCodeError = await deleteCode({ email });
        if (deleteCodeError) logWarning({ error: deleteCodeError, message: `Ошибка удаления кода` });
        return errorResponse(res, { defaultMessage: `Превышено количество попыток ввода кода` });
      }

      const increaseAttemptError = await increaseCodeSendingAttempt({ email });

      if (increaseAttemptError) {
        return errorResponse(res, { error: increaseAttemptError, defaultMessage: `Ошибка во время считывания кода` });
      }

      const _code = _.get(codeRow, `code`);
      if (!_code) return errorResponse(res, { defaultMessage: `Ошибка получения кода` });
      if (code !== _code) {
        return errorResponse(res, {
          data: { attemptsToBlock: MAX_CODE_ATTEMPTS - attempts },
          defaultMessage: `Неверный код`,
        });
      }

      const increaseRating = competition === `child` ? increaseChildWorkRating : increaseArchitectWorkRating;
      const [increaseError, increaseResponse] = await increaseRating({ id });

      if (increaseError) {
        return errorResponse(res, { error: increaseError, defaultMessage: `Ошибка во время обновления рейтинга` });
      }

      if (!_.get(increaseResponse, `affectedRows`)) {
        return errorResponse(res, { defaultMessage: `Не найдена работа, за которую вы хотите проголосовать` });
      }

      const addHistoryRowError = await addVotesHistoryRow({
        email,
        competition,
        workId: id,
        createTimestamp: +new Date(),
      });

      if (addHistoryRowError) {
        logWarning({ error: addHistoryRowError, message: `Ошибка записи строки с историей голосования` });
      }

      const deleteCodeError = await deleteCode({ email });
      if (deleteCodeError) logWarning({ error: deleteCodeError, message: `Ошибка удаления кода` });

      res.status(200).json({ ok: true, message: `Ваш голос учтен` });
    } catch (e) {
      return errorResponse(res, { error: e, defaultMessage: `Ошибка во время обработки запроса` });
    }
  },
};

const _ = require(`lodash`);
const passport = require(`passport`);
const LocalStrategy = require(`passport-local`).Strategy;
const { Strategy: JwtStrategy, ExtractJwt } = require(`passport-jwt`);
const { getUserByLogin } = require(`../db/kotelnikovo/admin.model`);
const passwordCrypt = require(`../utils/passwordCrypt`);
const jwt = require(`jsonwebtoken`);

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
};

passport.use(new LocalStrategy({
  usernameField: `username`,
  passwordField: `password`,
}, async (username, password, done) => {
  try {
    console.log(username, password);
    const userResult = getUserByLogin(username);
    if (!_.size(userResult)) return done(null, false);
    const user = _.first(userResult);
    const _password = _.get(user, `password`);
    if (password !== passwordCrypt.decrypt(_password)) return done(null, false);
    return done(null, user);
  } catch (e) {
    return done(e);
  }
}));

passport.use(new JwtStrategy(options, async (jwtPayload, next) => {
  try {
    const user = await getUserByLogin(_.get(jwtPayload, `login`));
    if (!_.size(user)) return next(null, false);
    return next(null, user);
  } catch (e) {
    return next(e);
  }
}));

passport.getToken = (payload) => jwt.sign(payload, options.secretOrKey);

module.exports = passport;

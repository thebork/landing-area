const css = require('@zeit/next-css');
const sass = require('@zeit/next-sass');
const withPlugins = require('next-compose-plugins');

const path = require(`path`);

module.exports = withPlugins([css, sass], {
  webpack(config, { dev }) {
    if (dev) {
      config.module.rules.push({
        test: /\.js$/,
        enforce: `pre`,
        exclude: /node_modules/,
        loader: `eslint-loader`,
        options: {
          // Emit errors as warnings for dev to not break webpack build.
          // Eslint errors are shown in console for dev, yay :-)
          emitWarning: dev,
        },
      })
    }

    config.resolve.modules.push(path.resolve('./'))
    return config
  }
});

import React, { useContext } from 'react';
import b_ from 'b_';
import _ from 'lodash';
import { Element } from 'react-scroll';
import { getGlobalPadding, parseJsonData } from '../../helper';

import Numbers01 from '../../partitial/Numbers01';
import Text01 from '../../partitial/Text01';
import IconList01 from '../../partitial/IconList01';

import './style.scss';
import { ProjectContext } from "../../../models/contexts";

export const b = b_.lock(`Features03`);

/* Data structure
{
  "title": "Block title",
  "items": {
    "digitFeatures": [
      {
        "number": 112,
        "prefix": "prefix or null ",
        "suffix": " suffix or null",
        "description": "Number description"
      },
      {
        "number": 112,
        "prefix": "prefix or null ",
        "suffix": " suffix or null",
        "description": "Number description"
      }
    ],
    "description": {
      "image": "Main area logo",
      "text": "Main area text"
    },
    "iconFeatures": [
      {
        "icon": "Icon filename",
        "description": "Icon escription"
      },
      {
        "icon": "Icon filename",
        "description": "Icon escription"
      }
    ]
  }
}
 */

const Features03 = ({
  primaryTitle, secondaryTitle, numbersData, featuresData, blockLogo, blockText, data, ...props
}) => {
  const _numbersData = parseJsonData(numbersData);
  const _featuresData = parseJsonData(featuresData);

  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  return (
    <Element name="about">
      <div className={b()} style={getGlobalPadding(globalPadding)}>
        <h2>{primaryTitle}</h2>
        <Numbers01 items={_numbersData} {...props} />
        <Text01 text={blockText} {...props} />
        <h2>{secondaryTitle}</h2>
        <IconList01 items={_featuresData} {...props} />
      </div>
    </Element>
  );
};

export default Features03;

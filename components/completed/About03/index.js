import React, { useContext, useState } from 'react';
import b_ from 'b_';
import _ from 'lodash';

import { getGlobalPadding } from 'components/helper';

import Paragraph01 from "components/partitial/Paragraph01";

import './style.scss';
import { ProjectContext } from "../../../models/contexts";

const b = b_.lock(`About03`);

const About03 = ({
  title, about, goals, persons,
}) => {
  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  const [selectedItem, setSelectedItem] = useState(0);

  return (
    <div className={b()} style={getGlobalPadding(globalPadding)}>
      <h2>{title}</h2>
      <div className={b(`content`)}>
        <Paragraph01
          item={{ name: `Описание`, content: about }}
          isOpened={selectedItem === 0}
          setOpenedState={() => setSelectedItem(0)}
        />
        <Paragraph01
          item={{ name: `Цели`, content: goals }}
          isOpened={selectedItem === 1}
          setOpenedState={() => setSelectedItem(1)}
        />
        <Paragraph01
          item={{ name: `Участники`, content: persons }}
          isOpened={selectedItem === 2}
          setOpenedState={() => setSelectedItem(2)}
        />
      </div>
    </div>
  );
};

export default About03;

import React, { useContext } from 'react';
import b_ from 'b_';
import _ from 'lodash';

import { ProjectContext } from "models/contexts";
import { getGlobalPadding } from "components/helper";

import Dropdown01 from "components/partitial/Dropdown01";

import './style.scss';

const b = b_.lock(`Footer01`);

const Footer01 = ({
  blockLogo, blockTitle, phoneText, phoneLink, address, socialNetworks, dropdownItems,
}) => {
  let _socialNetworks;
  try {
    if (typeof socialNetworks === `string`) {
      _socialNetworks = JSON.parse(socialNetworks);
    } else {
      _socialNetworks = socialNetworks;
    }
  } catch (e) {
    _socialNetworks = [];
  }
  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  return (
    <div className={b()} style={getGlobalPadding(globalPadding)}>
      <div className={b(`logo`)}>
        <img
          alt={blockTitle}
          className={b(`logo-image`)}
          src={`/uploads/images/${blockLogo}`}
        />
        <div className={b(`logo-description`)}>{blockTitle}</div>
      </div>
      <div className={b(`contacts`)}>
        <div className={b(`contacts-title`)}>Контакты:</div>
        <a
          className={b(`contacts-phone`)}
          href={`tel:${phoneLink}`}
          target="_blank"
        >
          {phoneText}
        </a>
        <div className={b(`contacts-address`)}>{address}</div>
      </div>
      <div className={b(`social`)}>
        <div className={b(`social-title`)}>Мы в соцсетях:</div>
        <div className={b(`social-links`)}>
          {
            _socialNetworks.map((item) => (
              <a
                key={item.type}
                style={{
                  background: `url('/uploads/images/${item.icon}') center no-repeat`,
                  backgroundSize: `contain`,
                }}
                className={b(`social-links-item`)}
                href={item.link}
                rel="noreferrer noopener"
                target="_blank"
              >
                <span className="text">{item.type}</span>
              </a>
            ))
          }
        </div>
      </div>
      <div className={b(`publish`)}>
        <Dropdown01
          name="Разместить работу"
          items={dropdownItems}
          xPosition="left"
          yPosition="top"
        />
      </div>
    </div>
  );
};

export default Footer01;

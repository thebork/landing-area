import React, {
  useState, useEffect, useRef, useContext,
} from 'react';
import b_ from 'b_';
import _ from 'lodash';

import Photo from './Photo';

import { getRefWidth } from '../../helper';

import './style.scss';
import { ProjectContext } from "../../../models/contexts";

const b = b_.lock(`Gallery02`);

/* Data structure
[`array`, `of`, `values`]
 */

const ACTIVE_PHOTO_WIDTH = 70; // Percents

const Gallery02 = ({ data }) => {
  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  const [selectedItem, setSelectedItem] = useState(0);
  const [blockWidth, setBlockWidth] = useState(0);
  const [isOnePhotoInRow, setIsOnePhotoInRow] = useState(false);

  const blockRef = useRef(null);

  const handleResize = () => {
    const _blockWidth = getRefWidth(blockRef);
    const _isOnePhotoInRow = _blockWidth <= 800;
    if (_isOnePhotoInRow !== isOnePhotoInRow) setIsOnePhotoInRow(_isOnePhotoInRow);
    setBlockWidth(_blockWidth);
  };

  useEffect(() => {
    handleResize();
    window.addEventListener(`resize`, handleResize);
    return () => window.removeEventListener(`resize`, handleResize);
  });

  const galleryWidth = blockWidth ? blockWidth - (globalPadding * 2) : 0;


  const nextPhoto = () => {
    if (selectedItem === _.size(data) - 1) {
      setSelectedItem(0);
      return;
    }
    setSelectedItem(selectedItem + 1);
  };

  const prevPhoto = () => {
    if (selectedItem === 0) {
      setSelectedItem(_.size(data) - 1);
      return;
    }
    setSelectedItem(selectedItem - 1);
  };

  const photosAmount = _.size(data);

  return (
    <div className={b()} ref={blockRef}>
      <div className={b(`title`)}>
        <h2>Фотографии</h2>
      </div>
      <div
        className={b(`gallery`)}
        style={{
          width: galleryWidth,
          height: isOnePhotoInRow ? (galleryWidth / 16 * 9) : ((galleryWidth / 100 * ACTIVE_PHOTO_WIDTH) / 16 * 9),
        }}
      >
        {
          data.map((item, index) => (
            <Photo
              key={item}
              activeWidth={isOnePhotoInRow ? 100 : ACTIVE_PHOTO_WIDTH}
              item={item}
              index={index}
              isOnePhotoInRow={isOnePhotoInRow}
              photosAmount={photosAmount}
              galleryWidth={galleryWidth}
              selectedItem={selectedItem}
              prevPhoto={prevPhoto}
              nextPhoto={nextPhoto}
            />
          ))
        }
      </div>
    </div>
  );
};

export default Gallery02;

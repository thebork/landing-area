import React, { useContext } from 'react';
import b_ from 'b_';
import _ from "lodash";
import cn from 'classnames';
import { ProjectContext } from "models/contexts";

import './style.scss';

const MARGIN = 2; // percents

const b = b_.lock(`Photo`);

const Photo = ({
  activeWidth,
  index,
  isOnePhotoInRow,
  photosAmount,
  galleryWidth,
  selectedItem,
  prevPhoto,
  nextPhoto,
  item,
}) => {
  const siteContext = useContext(ProjectContext);
  const projectName = _.get(siteContext, `projectName`);
  const isHiddenLeft = () => {
    if (selectedItem === 0) return isOnePhotoInRow ? index === photosAmount - 1 : index === photosAmount - 2;
    if (selectedItem === 1) return isOnePhotoInRow ? index === 0 : index === photosAmount - 1;
    return isOnePhotoInRow ? index === selectedItem - 1 : index === selectedItem - 2;
  };

  const isHiddenRight = () => {
    if (selectedItem === photosAmount - 1) return isOnePhotoInRow ? index === 0 : index === 1;
    if (selectedItem === photosAmount - 2) return isOnePhotoInRow ? index === photosAmount : index === 0;
    return isOnePhotoInRow ? index === selectedItem + 1 : index === selectedItem + 2;
  };

  const isSmallLeft = () => {
    if (isOnePhotoInRow) return false;
    if (selectedItem === 0) return index === photosAmount - 1;
    return index === selectedItem - 1;
  };

  const isSmallRight = () => {
    if (isOnePhotoInRow) return false;
    if (selectedItem === photosAmount - 1) return index === 0;
    return index === selectedItem + 1;
  };

  const getItemType = () => {
    if (index === selectedItem) return `active`;
    if (isSmallLeft()) return `smallLeft`;
    if (isSmallRight()) return `smallRight`;
    if (isHiddenLeft()) return `hiddenLeft`;
    if (isHiddenRight()) return `hiddenRight`;
    return `disabled`;
  };

  const itemType = getItemType();

  const getItemStyle = () => {
    if (itemType === `disabled`) return { display: `none` };
    let scaleValue = 1;
    let translateValue = 0;

    const onePercentInPixels = galleryWidth / 100;
    const itemWidth = onePercentInPixels * activeWidth;
    const itemHeight = itemWidth / 16 * 9;
    const marginPx = onePercentInPixels * MARGIN;

    if (itemType !== `active`) {
      const smallItemWidth = (galleryWidth - itemWidth - (marginPx * 2)) / 2;
      scaleValue = smallItemWidth / itemWidth;
      if (itemType.endsWith(`Left`)) translateValue = `-`;
      if (itemType.startsWith(`small`)) {
        translateValue += `${(galleryWidth / 2) - (smallItemWidth / 2)}px`;
      } else {
        translateValue += `${galleryWidth}px`;
      }
    }

    return {
      width: itemWidth,
      height: itemHeight,
      left: `calc(50% - ${itemWidth / 2}px`,
      transform: `translateX(${translateValue}) scale(${scaleValue})`,
    };
  };

  const onClickFunction = () => {
    if (itemType === `smallLeft`) return prevPhoto;
    if (itemType === `smallRight`) return nextPhoto;
    return _.noop;
  };

  return (
    <>
      {
        itemType !== `disabled`
        && (
          <div
            className={cn(b(), { active: itemType === `active` })}
            style={getItemStyle()}
            onClick={onClickFunction(index)}
          >
            { isOnePhotoInRow && (
              <div className={b(`previous`)} onClick={() => prevPhoto()}><div className={b(`previous-icon`)} /></div>
            )}
            <img alt={item} className={b(`image`)} src={`/uploads/photos/${item}`} />
            { isOnePhotoInRow && (
              <div className={b(`next`)} onClick={() => nextPhoto()}><div className={b(`next-icon`)} /></div>
            )}
          </div>
        )
      }
    </>
  );
};

export default Photo;

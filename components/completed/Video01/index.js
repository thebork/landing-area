import React, {
  useRef, useState, useEffect, useContext,
} from 'react';
import b_ from 'b_';
import _ from 'lodash';
import cn from 'classnames';
import YouTube from 'react-youtube';
import { Element } from 'react-scroll';

import './style.scss';
import { ProjectContext } from "models/contexts";
import { getGlobalPadding, parseJsonData } from "components/helper";

const PREVIEW_WIDTH = 240; // px
const ACTIVE_WIDTH = 800; // px
const X_MARGIN = 10; // px
const Y_MARGIN = 20; // px
const ANIMATION_DURATION = 500; // ms

export const b = b_.lock(`Video01`);

const Video01 = ({ data }) => {
  const videoData = parseJsonData(data);
  const videoBlockRef = useRef(null);
  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  const [videoBlockWidth, setVideoBlockWidth] = useState(0);
  const [activeVideoWidth, setActiveVideoWidth] = useState(ACTIVE_WIDTH);
  const activeVideoHeight = activeVideoWidth / 16 * 9;
  const [previewVideoWidth, setPreviewVideoWidth] = useState(PREVIEW_WIDTH);
  const previewVideoHeight = previewVideoWidth / 16 * 9;
  const [listOffset, setListOffset] = useState(0);
  const [selectedItem, setSelectedItem] = useState(0);
  const [isVideoPlayed, setVideoPlayedState] = useState(false);
  const [isWaitAnimation, setWaitAnimationState] = useState(false);
  const [player, setPlayer] = useState(null);
  const isSelectedItem = typeof selectedItem === `number`;
  const videosAmount = _.size(videoData);
  const listWidth = (previewVideoWidth + (X_MARGIN - 1)) * (isSelectedItem ? (videosAmount - 1) : videosAmount);
  const isListWide = listWidth > videoBlockWidth;

  const handleResize = () => {
    if (!videoBlockRef) return;
    const _videoBlockWidth = _.get(videoBlockRef, `current.offsetWidth`);
    if (_videoBlockWidth === videoBlockWidth) return;
    const _listOffset = listWidth > _videoBlockWidth ? 0 : ((_videoBlockWidth - listWidth) / 2);
    if (listOffset !== _listOffset) setListOffset(_listOffset);
    if (_videoBlockWidth < 800) setActiveVideoWidth(_videoBlockWidth);
    setVideoBlockWidth(_videoBlockWidth);
  };

  useEffect(() => {
    handleResize();
    window.addEventListener(`resize`, handleResize);
    return () => window.removeEventListener(`resize`, handleResize);
  });

  const listScrollLeft = () => {
    const translateValue = previewVideoWidth + X_MARGIN;
    setListOffset((listOffset + translateValue > 0) ? 0 : listOffset + translateValue);
  };

  const listScrollRight = () => {
    const translateValue = previewVideoWidth + X_MARGIN;
    const isOverScrolled = listWidth + listOffset - translateValue <= videoBlockWidth;
    setListOffset(isOverScrolled ? videoBlockWidth - listWidth : listOffset - translateValue);
  };

  const itemStyle = (index) => {
    const isActiveItem = selectedItem === index;
    const activeItemOffset = (videoBlockWidth - activeVideoWidth) / 2;

    const itemWidth = isActiveItem ? activeVideoWidth : previewVideoWidth;
    const itemHeight = isActiveItem ? activeVideoHeight : previewVideoHeight;

    let leftOffset;
    if (isActiveItem) {
      leftOffset = activeItemOffset;
    } else {
      leftOffset = !isSelectedItem || selectedItem > index
        ? (previewVideoWidth + X_MARGIN) * index
        : (previewVideoWidth + X_MARGIN) * (index - 1);
      leftOffset += listOffset;
    }

    const topOffset = isActiveItem ? 0 : activeVideoHeight + Y_MARGIN;

    return {
      left: leftOffset, top: topOffset, width: itemWidth, height: itemHeight,
    };
  };

  const playerOptions = {
    width: activeVideoWidth,
    height: activeVideoHeight,
    playerVars: {
      autoplay: 1,
    },
  };

  const onPlayerReady = (e) => {
    setPlayer(e.target);
    if (!isVideoPlayed) {
      e.target.pauseVideo();
    }
  };

  const selectItem = (index) => {
    if (selectedItem === index && isVideoPlayed) return;
    if (!isVideoPlayed && player) {
      setVideoPlayedState(true);
      player.playVideo();
    }
    setWaitAnimationState(true);
    setSelectedItem(index);
  };

  const afterAnimation = () => {

  };

  useEffect(() => {
    if (!isWaitAnimation) {
      afterAnimation();
      return;
    }

    const timer = setTimeout(() => {
      setWaitAnimationState(false);
    }, ANIMATION_DURATION);
    return () => {
      clearTimeout(timer);
    };
  }, [isWaitAnimation]);

  return (
    <Element name="video">
      <div className={b()} style={getGlobalPadding(globalPadding)}>
        <h2>Видеогалерея</h2>
        <div
          ref={videoBlockRef}
          className={b(`list`)}
          style={{ height: activeVideoHeight + Y_MARGIN + previewVideoHeight }}
        >
          <div className={cn(b(`list-controls`), { hidden: !isListWide })}>
            <div
              className={cn(b(`list-controls-scrollLeft`), { hidden: listOffset === 0 })}
              onClick={() => listScrollLeft()}
            >
              <div className={b(`list-controls-scrollLeft-icon`)} />
            </div>
            <div
              className={cn(b(`list-controls-scrollRight`), { hidden: listWidth + listOffset <= videoBlockWidth })}
              onClick={() => listScrollRight()}
            >
              <div className={b(`list-controls-scrollRight-icon`)} />
            </div>
          </div>
          {
            videoData.map((item, index) => (
              <div
                key={item.videoId}
                className={cn(b(`list-item`), {
                  active: selectedItem === index,
                  played: selectedItem === index && isVideoPlayed,
                })}
                style={itemStyle(index)}
                onClick={() => {
                  selectItem(index);
                }}
              >
                <img
                  alt="Video"
                  className={
                    cn(
                      b(`list-item-preview`),
                      { hidden: selectedItem === index && !isWaitAnimation && player && isVideoPlayed },
                    )
                  }
                  src={`https://img.youtube.com/vi/${item.videoId}/mqdefault.jpg`}
                />
                {
                  selectedItem === index
                  && (
                    <YouTube
                      videoId={item.videoId}
                      opts={playerOptions}
                      onReady={onPlayerReady}
                      crossorigin
                    />
                  )
                }
              </div>
            ))
          }

        </div>
      </div>
    </Element>
  );
};

export default Video01;

import React, { useContext, useState, useEffect, useRef } from 'react';
import b_ from 'b_';

import { getGlobalPadding, getRefHeight } from 'components/helper';

import { ProjectContext } from "models/contexts";

import './style.scss';
import _ from "lodash";

const b = b_.lock(`Title04`);

/* Data structure
{
  "primaryTitle": "Title text",
  "secondaryTitle": "Title text",
  "imageLogo": "Image file"
  "imageBackground": "Image file"
  "backgroundType": "center|fixed|etc"
}
 */

const Title04 = ({
  primaryTitle, secondaryTitle, imageLogo, imageBackground, backgroundType = `fixed`,
}) => {
  const siteContext = useContext(ProjectContext);
  const projectName = _.get(siteContext, `projectName`);
  const globalPadding = _.get(siteContext, `globalPadding`);

  const [logoHeight, setLogoHeight] = useState(0);
  const logoRef = useRef(null);

  useEffect(() => {
    setLogoHeight(getRefHeight(logoRef));
  }, []);

  return (
    <div
      className={b()}
      style={{
        backgroundImage: `url('/uploads/images/${imageBackground}')`,
        backgroundRepeat: `no-repeat`,
        backgroundSize: `cover`,
        // minHeight: logoHeight + HEADER_HEIGHT,
        ...getGlobalPadding(globalPadding),
      }}
    >
      <div className={b(`text`)}>
        <h1>{primaryTitle}</h1>
        { secondaryTitle && <h3>{secondaryTitle}</h3> }
      </div>
      <div className={b(`logo`)}>
        <img alt="Logo" ref={logoRef} src={`/uploads/images/${imageLogo}`} />
      </div>
    </div>
  );
};

export default Title04;

import _ from 'lodash';

export const getGlobalPadding = (value) => (
  value ? { paddingLeft: `${value}px`, paddingRight: `${value}px`, boxSizing: `border-box` } : {}
);

export const getRefWidth = (ref) => _.get(ref, `current.offsetWidth`, 0);
export const getRefHeight = (ref) => _.get(ref, `current.offsetWidth`, 0);

export const parseYoutubeVideoId = (url) => {
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  const match = url.match(regExp);
  return (match && match[7].length === 11) ? match[7] : false;
};

export const parseResponse = ({
  response,
  dataPath = `data`,
  normalize,
  defaultError = `Во время запроса произошла ошибка`,
}) => {
  const status = _.get(response, `status`);
  const responseError = _.get(response, `data.error`);
  const error = responseError || defaultError;
  if (status !== 200 || responseError) throw error;
  const data = _.get(response, `data.${dataPath}`);

  return _.isFunction(normalize) ? normalize(data) : data;
};

export const parseError = ({
  error,
  defaultError = `Во время запроса произошла ошибка`,
}) => {
  const response = _.get(error, `response.data`);
  if (!response) return defaultError;
  const errorMessage = _.get(response, `message`);
  return errorMessage || defaultError;
};

export const formatAge = (age) => {
  if (!age || !_.toString(age).length) return age;
  const lastSymbol = _.toString(age).substr(-1);
  const lastDigit = _.toFinite(lastSymbol);
  if (lastDigit === 1) return `${age} год`;
  return (lastDigit > 1 && lastDigit < 4) ? `${age} года` : `${age} лет`;
};

export const toCamelCase = (src) => {
  function camelCaser(sourceObj) {
    const camelCasedObj = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, value] of Object.entries(sourceObj)) {
      camelCasedObj[_.camelCase(key)] = value;
    }
    return camelCasedObj;
  }
  if (_.isPlainObject(src)) return camelCaser(src);
  if (_.isArray(src)) return src.map((item) => camelCaser(item));
};

export const orderSiteData = (data) => {
  const camelCasedData = toCamelCase(data);
  return camelCasedData.sort((a, b) => _.toFinite(a.sortOrder) - _.toFinite(b.sortOrder));
};

export const sortSiteData = (data) => {
  const camelCasedData = toCamelCase(data);
  const orderedData = orderSiteData(camelCasedData);
  const sortedObj = _.reduce(orderedData, (result, item) => {
    const blockName = _.get(item, `block`);
    // eslint-disable-next-line no-param-reassign
    delete item.block;
    // eslint-disable-next-line no-unused-expressions,no-param-reassign
    result[blockName] ? result[blockName].push(item) : result[blockName] = [item];
    return result;
  }, {});
  const sortedArray = [];
  Object.entries(sortedObj).forEach(([key, value]) => {
    sortedArray.push({ name: key, fields: toCamelCase(value) });
  });
  return sortedArray;
};

export const addAccessors = (list) => {
  const _addAccessors = (item) => {
    const objectWithAccessors = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, value] of Object.entries(item)) {
      objectWithAccessors[`_${key}`] = value;
      Object.defineProperty(objectWithAccessors, key, {
        get() {
          return this[`_${key}`];
        },
        set(newValue) {
          this[`_${key}`] = newValue;
        },
      });
    }
    return objectWithAccessors;
  };
  if (_.isPlainObject(list)) return _addAccessors(list);
  if (_.isArray(list)) return list.map((item) => _addAccessors(item));
  return list;
};

export const normalizeSiteData = (data) => _.reduce(data, (result, { value, slug }) => {
  const key = _.camelCase(slug);
  return {
    ...result,
    [key]: value,
  };
}, {});

export const normalizeChildWorks = (items) => _.map(items, (item) => ({
  id: _.get(item, `id`),
  person: _.get(item, `name`),
  age: formatAge(_.get(item, `age`)),
  studyPlace: _.get(item, `study_place`),
  aboutWork: _.get(item, `work_about`),
  work: _.get(item, `work_file`),
  rating: _.get(item, `votes`),
}));

export const normalizeArchitectWorks = (items) => _.map(items, (item) => ({
  id: _.get(item, `id`),
  photo: _.get(item, `photo_file`),
  person: _.get(item, `name`),
  age: formatAge(_.get(item, `age`)),
  studyPlace: _.get(item, `study_place`),
  aboutWork: _.get(item, `work_about`),
  work: _.get(item, `work_file`),
  rating: _.get(item, `votes`),
}));

export const parseJsonData = (data) => {
  let _data;
  try {
    if (typeof data === `string`) {
      _data = JSON.parse(data);
    } else {
      _data = data;
    }
  } catch (e) {
    _data = [];
  }
  return _data;
};

import React from 'react';
import b_ from 'b_';
import { CSSTransition } from 'react-transition-group';

import './style.scss';

const b = b_.lock(`Transition`);

const MountTransition = ({ mounted, children }) => (
  <CSSTransition
    in={mounted}
    timeout={1000}
    classNames={{
      enter: b(`enter`),
      enterActive: b(`enterActive`),
      exit: b(`exit`),
      exitActive: b(`exitActive`),
    }}
    unmountOnExit
  >
    {children}
  </CSSTransition>
);

export default MountTransition;

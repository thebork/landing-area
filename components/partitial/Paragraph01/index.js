import React, { useRef } from 'react';
import b_ from 'b_';
import _ from 'lodash';
import cn from 'classnames';

import './style.scss';

const Paragraph01 = ({
  item = {},
  isOpened,
  setOpenedState,
}) => {
  const b = b_.lock(`Paragraph01`);

  const contentRef = useRef(null);
  const contentHeight = _.get(contentRef, `current.offsetHeight`);

  return (
    <article className={b()}>
      <div
        className={cn(b(`paragraph`), { opened: isOpened })}
      >
        <header>
          <h3 className={b(`paragraph-title`)} onClick={() => setOpenedState(!isOpened)}>{item.name}</h3>
        </header>
        <div className={b(`paragraph-text`)} style={{ maxHeight: isOpened ? contentHeight : 0 }}>
          <p ref={contentRef}>{item.content}</p>
        </div>
      </div>
    </article>
  );
};

export default Paragraph01;

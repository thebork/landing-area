import React from 'react';
import b_ from 'b_';

import './style.scss';

const Text01 = ({ text }) => {
  const b = b_.lock(`Text01`);
  return (
    <div className={b()}>
      <div className={b(`text`)}>
        {text}
      </div>
    </div>
  );
};

export default Text01;

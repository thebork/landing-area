import React, { useContext } from 'react';
import b_ from 'b_';
import _ from 'lodash';
import { ProjectContext } from "models/contexts";

import './style.scss';

const Item = ({ b, item }) => {
  const siteContext = useContext(ProjectContext);
  const projectName = _.get(siteContext, `projectName`);
  let fontSize;
  const textLength = _.size(item.text);
  switch (true) {
    case textLength < 20:
      fontSize = 2; // rem
      break;
    case textLength > 20 && textLength < 40:
      fontSize = 1.8; // rem
      break;
    case textLength >= 40 && textLength < 60:
      fontSize = 1.6; // rem
      break;
    case textLength >= 60 && textLength < 80:
      fontSize = 1.4; // rem
      break;
    case textLength >= 80 && textLength < 100:
      fontSize = 1.2; // rem
      break;
    case textLength >= 100:
      fontSize = 1; // rem
      break;
  }
  return (
    <div className={b(`item`)}>
      <div className={b(`item-icon`)}>
        <img alt={item.text} src={`/uploads/images/${item.icon}`} />
      </div>
      <div
        className={b(`item-description`)}
        style={{ fontSize: `${fontSize}rem` }}
      >
        {item.text}
      </div>
    </div>
  );
};

const IconList01 = ({ items }) => {
  const b = b_.lock(`IconList01`);
  return (
    <div className={b()}>
      {
        items.map((item, index) => (
          <Item key={index} b={b} item={item} />
        ))
      }
    </div>
  );
};

export default IconList01;

import React, { useContext } from 'react';
import b_ from 'b_';

import { ProjectContext } from "models/contexts";

import './style.scss';
import _ from "lodash";

const ImageWithText01 = ({ logo, text }) => {
  const b = b_.lock(`ImageWithText01`);
  const siteContext = useContext(ProjectContext);
  const projectName = _.get(siteContext, `projectName`);
  return (
    <div className={b()}>
      <div className={b(`image`)}>
        <img alt="Icon" src={`/images/${projectName}/${logo}`} />
      </div>
      <div className={b(`text`)}>
        {text}
      </div>
    </div>
  );
};

export default ImageWithText01;

import React from 'react';
import b_ from 'b_';

import './style.scss';

const ButtonClose = ({ onClick }) => {
  const b = b_.lock(`ButtonClose`);
  return (
    <div className={b()} onClick={onClick}>
      <div className={b(`icon`)} />
    </div>
  );
};

export default ButtonClose;

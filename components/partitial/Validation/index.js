import React, { useState } from 'react';
import b_ from 'b_';
import { toast } from 'react-toastify';

import { requestCode, sendCode as sendCodeApi } from "models/kotelnikovo/api";
import { parseError, parseResponse } from "components/helper";

import withOverlay from 'components/hocs/withOverlay';
import ButtonClose from 'components/partitial/ButtonClose';
import Screen from './Screen';

import './style.scss';

const Validation = ({ competition, id, onClose }) => {
  const b = b_.lock(`Validation`);

  const [step, setStep] = useState(`email`);
  const [email, setEmail] = useState(``);
  const [code, setCode] = useState(``);

  const sendEmail = async () => {
    try {
      parseResponse({
        response: await requestCode({ email, competition: `child` }),
      });
      setStep(`code`);
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  const sendCode = async () => {
    try {
      parseResponse({
        response: await sendCodeApi({
          email, competition, id, code,
        }),
      });
      toast.success(`Ваш голос учтен`);
      onClose();
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  return (
    <div className={b()}>
      <ButtonClose onClick={onClose} />
      {
        step === `email` && (
          <Screen
            id={step}
            title="Введите email"
            type="email"
            value={email}
            setValue={setEmail}
            onSubmit={sendEmail}
            buttonText="Отправить код"
          />
        )
      }
      {
        step === `code` && (
          <Screen
            id={step}
            title="Введите код"
            type="text"
            value={code}
            setValue={setCode}
            onSubmit={sendCode}
            buttonText="Проголосовать"
          />
        )
      }
    </div>
  );
};

export default withOverlay(Validation);

import React from 'react';
import b_ from 'b_';

import { Row, Input, Button } from 'components/ui/SimpleKit';

import './style.scss';

const Screen = ({
  id, title, type, value, setValue, buttonText, onSubmit,
}) => {
  const b = b_.lock(`Screen`);
  return (
    <div className={b()}>
      <h2>{title}</h2>
      <Row>
        <Input id={id} type={type} onChange={setValue} value={value} />
      </Row>
      <Row>
        <Button onClick={onSubmit}>{buttonText}</Button>
      </Row>
    </div>
  );
};

export default Screen;

import React from 'react';
import b_ from 'b_';
import cn from 'classnames';

import './style.scss';

const Controls01 = ({
  downloadLink, watchAction, voteAction, rating,
}) => {
  const b = b_.lock(`Controls01`);
  return (
    <div className={b()}>
      <div className={cn(b(`item`), `watch`)}>
        <span className={b(`item-link`)} onClick={watchAction}>Смотреть</span>
      </div>
      <div className={cn(b(`item`), `download`)}>
        <a className={b(`item-link`)} target="_blank" rel="noopener noreferer" href={downloadLink}>
          Скачать
        </a>
      </div>
      <div className={cn(b(`item`), `rating`)}>
        <span className={b(`item-info`)}>{`Голосов: ${rating}`}</span>
      </div>
      <div className={cn(b(`item`), `vote`)}>
        <span className={b(`item-link`)} onClick={voteAction}>Голосовать</span>
      </div>
    </div>
  );
};

export default Controls01;

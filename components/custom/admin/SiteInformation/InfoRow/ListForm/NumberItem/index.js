import React from 'react';
import { Button, Grid, TextField } from "@material-ui/core";

const NumberItem = ({
  id, data, updateValue, deleteValue,
}) => {
  const {
    prefix, suffix, number, description,
  } = data;
  const handleChange = (e) => {
    const key = e.target.getAttribute(`id`);
    updateValue({
      id,
      key,
      value: e.target.value,
    });
  };

  return (
    <>
      <Grid item xs={2}>
        <TextField
          id="prefix"
          label="Префикс"
          variant="outlined"
          size="small"
          value={prefix}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <TextField
          id="suffix"
          label="Суффикс"
          variant="outlined"
          size="small"
          value={suffix}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <TextField
          id="number"
          label="Число"
          variant="outlined"
          size="small"
          value={number}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          id="description"
          label="Описание"
          variant="outlined"
          size="small"
          value={description}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <Button variant="contained" color="secondary" fullWidth onClick={() => deleteValue({ id })}>Удалить</Button>
      </Grid>
    </>
  );
};

export default NumberItem;

import React, { useReducer } from 'react';
import _ from 'lodash';
import { toast } from 'react-toastify';
import { Box, Button, Grid } from "@material-ui/core";

import { parseCookies } from "nookies";
import { setSiteData } from "models/kotelnikovo/api";
import { parseError, parseResponse } from "components/helper";

import NumberItem from "./NumberItem";
import FeatureItem from "./FeatureItem";
import SocialItem from "./SocialItem";
import VideoItem from "./VideoItem";

function updateElement(state, payload) {
  const { id, key, value } = payload;
  const stateCopy = JSON.parse(JSON.stringify(state));
  stateCopy[id] = { ...stateCopy[id], [key]: value };
  return stateCopy;
}

function addEmptyElement(state) {
  if (!_.size(state)) return state;
  const stateCopy = JSON.parse(JSON.stringify(state));
  const newValue = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const [key] of Object.entries(stateCopy[0])) { newValue[key] = ``; }
  stateCopy.push(newValue);
  return stateCopy;
}

function reducer(state, action) {
  const stateCopy = JSON.parse(JSON.stringify(state));
  const payload = _.get(action, `payload`);
  switch (action.type) {
    case `update`:
      return updateElement(state, payload);
    case `add`:
      return addEmptyElement(state);
    case `delete`:
      if (_.size(stateCopy) <= 1) return stateCopy;
      stateCopy.splice(payload.id, 1);
      return stateCopy;
    default:
      return state;
  }
}

const ListForm = ({ type, data = [] }) => {
  let ItemComponent;
  switch (type) {
    case `numbers`:
      ItemComponent = NumberItem;
      break;
    case `features`:
      ItemComponent = FeatureItem;
      break;
    case `social`:
      ItemComponent = SocialItem;
      break;
    case `video`:
      ItemComponent = VideoItem;
      break;
  }

  let initialState;
  try {
    const list = _.get(data, `value`);
    initialState = JSON.parse(list);
  } catch (error) {
    initialState = [];
  }
  const [list, dispatch] = useReducer(reducer, initialState);

  const updateInformation = async () => {
    const name = _.get(data, `slug`);
    try {
      const cookies = parseCookies(null);
      const token = _.get(cookies, `token`);
      const stringifiedList = JSON.stringify(list);
      parseResponse({
        response: await setSiteData({
          name,
          value: stringifiedList,
          token,
        }),
      });
      toast.success(`Информация успешно обновлена`);
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  const updateValue = async ({ id, key, value }) => {
    dispatch({ type: `update`, payload: { id, key, value } });
  };

  const addValue = async () => {
    dispatch({ type: `add` });
  };

  const deleteValue = async ({ id }) => {
    dispatch({ type: `delete`, payload: { id } });
  };

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
      spacing={3}
    >
      {
        list && list.map((item, index) => (
          <ItemComponent key={index} id={index} data={item} updateValue={updateValue} deleteValue={deleteValue} />
        ))
      }
      <Grid
        item
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-start"
      >
        <Box mx={2}>
          <Button variant="contained" color="default" onClick={addValue}>Добавить элемент</Button>
        </Box>
        <Button variant="contained" color="primary" onClick={updateInformation}>Сохранить</Button>
      </Grid>
    </Grid>
  );
};

export default ListForm;

import React from 'react';
import { Button, Grid, TextField } from "@material-ui/core";

import { parseYoutubeVideoId } from 'components/helper';

const VideoItem = ({
  id, data, updateValue, deleteValue,
}) => {
  const { videoId } = data;
  const handleChange = (e) => {
    const key = e.target.getAttribute(`id`);
    const payload = e.target.value || ``;
    const _videoId = parseYoutubeVideoId(e.target.value) || payload;
    updateValue({
      id,
      key,
      value: _videoId,
    });
  };

  return (
    <>
      <Grid item xs={10}>
        <TextField
          id="videoId"
          label="ID или ссылка на видео"
          variant="outlined"
          size="small"
          value={videoId}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <Button variant="contained" color="secondary" fullWidth onClick={() => deleteValue({ id })}>Удалить</Button>
      </Grid>
    </>
  );
};

export default VideoItem;

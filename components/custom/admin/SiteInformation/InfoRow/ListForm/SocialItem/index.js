import React from 'react';
import { Button, Grid, TextField } from "@material-ui/core";

const SocialItem = ({
  id, data, updateValue, deleteValue,
}) => {
  const { icon, type, link } = data;
  const handleChange = (e) => {
    const key = e.target.getAttribute(`id`);
    updateValue({
      id,
      key,
      value: e.target.value,
    });
  };

  return (
    <>
      <Grid item xs={2}>
        <TextField
          id="type"
          label="Название"
          variant="outlined"
          size="small"
          value={type}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <TextField
          id="icon"
          label="Иконка"
          variant="outlined"
          size="small"
          value={icon}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          id="link"
          label="Ссылка"
          variant="outlined"
          size="small"
          value={link}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <Button variant="contained" color="secondary" fullWidth onClick={() => deleteValue({ id })}>Удалить</Button>
      </Grid>
    </>
  );
};

export default SocialItem;

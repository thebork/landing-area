import React from 'react';
import { Button, Grid, TextField } from "@material-ui/core";

const NumberItem = ({
  id, data, updateValue, deleteValue,
}) => {
  const { icon, text } = data;
  const handleChange = (e) => {
    const key = e.target.getAttribute(`id`);
    updateValue({
      id,
      key,
      value: e.target.value,
    });
  };

  return (
    <>
      <Grid item xs={3}>
        <TextField
          id="icon"
          label="Иконка"
          variant="outlined"
          size="small"
          value={icon}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={7}>
        <TextField
          id="text"
          label="Текст"
          variant="outlined"
          size="small"
          value={text}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={2}>
        <Button variant="contained" color="secondary" fullWidth onClick={() => deleteValue({ id })}>Удалить</Button>
      </Grid>
    </>
  );
};

export default NumberItem;

import React, { useState } from 'react';
import b_ from 'b_';
import _ from 'lodash';
import { toast } from 'react-toastify';
import { Button, Grid, TextField } from "@material-ui/core";

import { parseCookies } from "nookies";
import { setSiteData } from "models/kotelnikovo/api";
import { parseError, parseResponse } from "components/helper";

import './style.scss';

const Text = ({ data, multiline }) => {
  const b = b_.lock(`Text`);
  const _value = _.get(data, `value`);
  const [value, setValue] = useState(_value);

  const updateInformation = async () => {
    const name = _.get(data, `slug`);
    try {
      const cookies = parseCookies(null);
      const token = _.get(cookies, `token`);
      parseResponse({
        response: await setSiteData({
          name,
          value,
          token,
        }),
      });
      toast.success(`Информация успешно обновлена`);
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  const handleChange = async (e) => {
    setValue(e.target.value);
  };

  return (
    <div className={b()}>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-start"
      >
        <TextField
          id={data.slug}
          label={data.name}
          variant="outlined"
          size="small"
          margin="normal"
          multiline={multiline}
          rows={3}
          value={value}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-start"
      >
        <Button variant="contained" color="primary" onClick={updateInformation}>Сохранить</Button>
      </Grid>
    </div>
  );
};

export default Text;

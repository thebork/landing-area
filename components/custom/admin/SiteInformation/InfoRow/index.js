import React from 'react';

import ListForm from "./ListForm";
import Text from "./Text";

import './style.scss';

const InfoRow = ({ data }) => (
  <div>
    {
      (data.type === `numbers` || data.type === `features` || data.type === `social` || data.type === `video`)
      && <ListForm type={data.type} data={data} />
    }
    {data.type === `text` && <Text data={data} />}
    {data.type === `textarea` && <Text multiline data={data} />}
  </div>
);


export default InfoRow;

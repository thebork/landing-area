import React, { useState, useEffect } from 'react';
import _ from "lodash";
import { toast } from "react-toastify";

import { orderSiteData, parseError, parseResponse } from "components/helper";
import { getSiteData } from "models/kotelnikovo/api";

import InfoRow from "./InfoRow";

import './style.scss';

const SiteInformation = ({ area }) => {
  const [data, setData] = useState(null);
  const [shouldLoadingData, setShouldLoadingData] = useState(true);
  const fetchData = async () => {
    try {
      const response = parseResponse({
        response: await getSiteData(area),
        normalize: orderSiteData,
      });
      if (_.size(response)) {
        setData(response);
      }
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
    setShouldLoadingData(false);
  };

  useEffect(() => {
    if (!shouldLoadingData) return;
    fetchData();
  }, [shouldLoadingData]);

  useEffect(() => {
    if (shouldLoadingData) return;
    setShouldLoadingData(true);
    // return () => setShouldLoadingData(false);
  }, [area]);

  return (
    <div>
      {
        data && data.map((item, index) => (
          <InfoRow key={`${item.slug}${index}`} data={item} />
        ))
      }
    </div>
  );
};

export default React.memo(SiteInformation);

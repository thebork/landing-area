import React, { useState, useEffect } from 'react';
import b_ from 'b_';
import _ from "lodash";
import { toast } from "react-toastify";
import { parseCookies } from "nookies";
import { Button } from '@material-ui/core';

import { parseError, parseResponse } from "components/helper";
import {
  getPhotos,
  getFiles,
  uploadPhotos as uploadPhotosApi,
  uploadFiles as uploadFilesApi,
  removePhoto as removePhotoApi,
  removeFile as removeFileApi,
} from "models/kotelnikovo/api";

import './style.scss';

const b = b_.lock(`FileLoader`);

const FileLoader = ({ area }) => {
  const [data, setData] = useState(null);
  const [shouldLoadingData, setShouldLoadingData] = useState(true);
  const [isUploadingFiles, setUploadingFilesState] = useState(false);

  const getFromApi = area === `photos` ? getPhotos : getFiles;
  const uploadToApi = area === `photos` ? uploadPhotosApi : uploadFilesApi;
  const removeFromApi = area === `photos` ? removePhotoApi : removeFileApi;
  const fetchData = async () => {
    try {
      const cookies = parseCookies(null);
      const token = _.get(cookies, `token`);
      const response = parseResponse({
        response: await getFromApi({ token }),
      });
      if (_.size(response)) {
        setData(response);
      }
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
    setShouldLoadingData(false);
  };

  const removeFile = async (fileName) => {
    // eslint-disable-next-line no-restricted-globals,no-alert
    const isRemove = confirm(`Вы действительно хотите удалить файл?`);
    if (!isRemove) return;
    try {
      const cookies = parseCookies(null);
      const token = _.get(cookies, `token`);
      await removeFromApi({ file: fileName, token });
      setShouldLoadingData(true);
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  useEffect(() => {
    if (!shouldLoadingData) return;
    fetchData();
  }, [shouldLoadingData]);

  useEffect(() => {
    if (shouldLoadingData) return;
    setShouldLoadingData(true);
  }, [area]);

  const handleInput = async (e) => {
    const files = _.get(e, `target.files`);
    // eslint-disable-next-line no-alert
    if (!files) alert(`Ошибка в загрузке файлов`);
    setUploadingFilesState(true);
    try {
      const cookies = parseCookies(null);
      const token = _.get(cookies, `token`);
      const formData = new FormData();

      for (let i = 0; i < files.length; i++) {
        formData.append(`files`, files[i]);
      }

      await uploadToApi({ token, formData });
      setShouldLoadingData(true);
      toast.success(`Файлы успешно загружены`);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      toast.error(`Ошибка при загрузке файлов`);
    }
    setUploadingFilesState(false);
  };

  return (
    <div className={b()}>
      <div className={b(`upload`)}>
        <input
          className={b(`upload-input`)}
          accept="image/*"
          id="file-uploader"
          multiple
          type="file"
          name="files"
          onChange={handleInput}
        />
        <label htmlFor="file-uploader">
          <Button
            variant="contained"
            color="primary"
            disabled={isUploadingFiles}
            component="span"
          >
            { isUploadingFiles ? `Загружаем файлы` : `Загрузить файлы` }
          </Button>
        </label>
      </div>
      <div className={b(`list`)}>
        { data && data.map((item) => (
          <div key={item} className={b(`list-file`)}>
            <div className={b(`list-file-name`)}>{item}</div>
            <div className={b(`list-file-remove`)} onClick={() => removeFile(item)} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default FileLoader;

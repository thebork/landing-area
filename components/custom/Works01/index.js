import React, { useState, useEffect, useContext } from 'react';
import b_ from 'b_';
import _ from 'lodash';

import {
  getWorksCount,
  getWorksPage,
} from 'models/kotelnikovo/api';

import {
  getGlobalPadding,
  parseResponse,
  normalizeChildWorks,
  normalizeArchitectWorks,
} from 'components/helper';

import CompetitionRow01 from "components/custom/CompetitionRow01";
import CompetitionRow02 from "components/custom/CompetitionRow02";
import Controls01 from "components/custom/Controls01";
import Paginator01 from "components/partitial/Paginator01";

import './style.scss';
import PdfPopup01 from "components/completed/PdfPopup01";
import Validation from "../../partitial/Validation";
import MountTransition from "../../partitial/MountTransition";
import { ProjectContext } from "../../../models/contexts";

const filePrefix = `uploads/`;

const b = b_.lock(`Works01`);

const MOUNT_ANIMATION_DURATION = 1000; // ms

const Works01 = ({ competition, type }) => {
  const siteContext = useContext(ProjectContext);
  const globalPadding = _.get(siteContext, `globalPadding`);

  const [works, setWorks] = useState([]);
  const [isLoading, setLoadingState] = useState(false);
  const [isError, setErrorState] = useState(false);
  const [shouldLoadingData, setShouldLoadingData] = useState(true);

  const [selectedWork, setSelectedWork] = useState(null);
  const [selectedAction, setSelectedAction] = useState(null);
  const [isWorkClosing, setWorkClosingState] = useState(false);

  const isSelectedWork = !!_.size(selectedWork);

  const [pagesAmount, setPagesAmount] = useState(1);
  const [pageNumber, setPageNumber] = useState(1);

  const changePageNumber = (page) => {
    setPageNumber(page);
    setShouldLoadingData(true);
  };

  let RowComponent;
  let normalizeData;

  if (competition === `child`) {
    RowComponent = CompetitionRow01;
    normalizeData = normalizeChildWorks;
  } else {
    RowComponent = CompetitionRow02;
    normalizeData = normalizeArchitectWorks;
  }
  const fetchData = async (page) => {
    setLoadingState(true);
    setErrorState(false);
    try {
      const count = parseResponse({
        response: await getWorksCount({ competition }),
      });
      const _pagesAmount = _.get(count, `pagesAmount`);

      const fetchPage = _pagesAmount < page ? _pagesAmount : page;
      const response = parseResponse({
        response: await getWorksPage({ competition, page: fetchPage }),
        normalize: normalizeData,
      });
      setPagesAmount(_pagesAmount);
      setWorks(response);
    } catch {
      setErrorState(true);
    }
    setLoadingState(false);
  };

  useEffect(() => {
    if (!shouldLoadingData) return;
    setShouldLoadingData(false);
    fetchData(pageNumber);
  }, [shouldLoadingData]);

  const setWorkAndAction = (payload = { type: null, work: {} }) => {
    const { type, work } = payload;
    setSelectedWork(work);
    setSelectedAction(type);
  };

  const onCloseWork = () => {
    setWorkClosingState(true);
  };

  const closeWork = () => {
    setWorkAndAction();
    setWorkClosingState(false);
  };

  useEffect(() => {
    if (!isWorkClosing) return;
    const timer = setTimeout(() => closeWork(), MOUNT_ANIMATION_DURATION);
    return () => clearTimeout(timer);
  }, [isWorkClosing]);

  const closeWorkAndReload = () => {
    setWorkAndAction();
    setShouldLoadingData(true);
  };

  const isWorksExist = works && works.length;

  return (
    <div className={b()} style={getGlobalPadding(globalPadding)}>
      <div className={b(`title`)}><h3>Работы участников</h3></div>
      <div className={b(`works`)}>
        {
          !isLoading && isError && (
            <div className={b(`works-error`)}>
              <div className={b(`works-error-comment`)}>Ошибка загрузки.</div>
              <div className={b(`works-error-reload`)} onClick={() => setShouldLoadingData(true)}>
                Попробовать снова
              </div>
            </div>
          )
        }
        {
          !isLoading && !isError && !isWorksExist && (
            <div className={b(`works-empty`)}>
              <div className={b(`works-empty-comment`)}>Нет работ.</div>
            </div>
          )
        }
        {
          isWorksExist
            ? (
              <div className={b(`works-controls`)}>
                <Paginator01
                  activePage={pageNumber}
                  pagesAmount={pagesAmount}
                  setPage={changePageNumber}
                />
              </div>
            )
            : null
        }
        {
          isWorksExist
            ? works.map((item) => (
              <div key={item.id} className={b(`works-row`)}>
                <RowComponent item={item} className={b(`works-row-info`)} />
                <Controls01
                  className={b(`works-row-controls`)}
                  rating={item.rating}
                  watchAction={() => setWorkAndAction({ type: `watch`, work: item })}
                  voteAction={() => setWorkAndAction({ type: `vote`, work: item })}
                  downloadLink={filePrefix + item.work}
                />
              </div>
            ))
            : null
        }
        {
          isLoading && (
            <div className={b(`works-loading`)}>
              <div className={b(`works-loading-comment`)}>Загрузка...</div>
            </div>
          )
        }
      </div>
      <MountTransition mounted={selectedAction === `watch` && !isWorkClosing}>
        <PdfPopup01 link={isSelectedWork ? `${filePrefix}${selectedWork.work}` : null} onClose={onCloseWork} />
      </MountTransition>
      <MountTransition mounted={selectedAction === `vote`}>
        <Validation
          id={isSelectedWork ? _.get(selectedWork, `id`, null) : null}
          competition={competition}
          onClose={closeWorkAndReload}
        />
      </MountTransition>
    </div>
  );
};

export default Works01;

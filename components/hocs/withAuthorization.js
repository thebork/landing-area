import React, { Component } from 'react';
import _ from 'lodash';
import { parseCookies } from 'nookies';
import { parseResponse } from 'components/helper';
import { getUser } from 'models/kotelnikovo/api';

const withAuthorization = (WrappedComponent) => class Authorization extends Component {
  static async getInitialProps(ctx) {
    const userAgent = process.browser ? navigator.userAgent : ctx.req.headers[`user-agent`];
    const cookies = parseCookies(ctx);
    const token = _.get(cookies, `token`);
    if (!token) return ctx.res.redirect(`/kotelnikovo/login`);
    try {
      parseResponse({
        response: await getUser(token),
      });
    } catch (error) {
      return ctx.res.redirect(`/kotelnikovo/login`);
    }

    const pageProps = WrappedComponent.getInitialProps && await WrappedComponent.getInitialProps(ctx);

    return {
      ...pageProps,
      userAgent,
    };
  }

  render() {
    return <WrappedComponent {...this.props} />;
  }
};

export default withAuthorization;

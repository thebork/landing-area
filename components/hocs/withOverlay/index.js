import React from 'react';
import b_ from 'b_';

import './style.scss';

const b = b_.lock(`Overlay`);

const Overlay = (Component) => (props) => (
  <div className={b()}>
    <Component {...props} />
  </div>
);

export default Overlay;

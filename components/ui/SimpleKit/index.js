import React from 'react';
import cn from 'classnames';

import './style.scss';

const kitPrefix = `SimpleUI-`;

export const Input = ({
  id, type, onChange, value,
}) => (
  <input
    id={id}
    type={type}
    onChange={(e) => onChange(e.target.value)}
    value={value}
    className={`${kitPrefix}Text`}
  />
);

export const TextArea = ({ id, onChange, value }) => (
  <textarea id={id} onChange={onChange} value={value} className={`${kitPrefix}TextArea`} />
);

export const Button = ({ children, type, onClick }) => (
  <button
    type="button"
    onClick={onClick}
    className={cn(`${kitPrefix}Button`, type)}
  >
    {children}
  </button>
);

export const Row = ({ children }) => (<div className={`${kitPrefix}Row`}>{children}</div>);

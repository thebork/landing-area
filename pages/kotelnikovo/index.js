import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import _ from 'lodash';
import { Link, Element } from 'react-scroll';
import { toast } from 'react-toastify';

import { ProjectContext } from "models/contexts";

import { normalizeSiteData, parseResponse } from "components/helper";
import { getSiteData, getPhotos } from "models/kotelnikovo/api";

import jsonData from 'data/kotelnikovo/data.json';

import Menu02 from 'components/completed/Menu02';
import Title04 from 'components/completed/titles/Title04';
import Gallery02 from 'components/completed/Gallery02';
import About03 from 'components/completed/About03';
import Form02 from 'components/completed/Form02';
import Footer01 from 'components/completed/Footer01';
import Works01 from 'components/custom/Works01';
import Video01 from 'components/completed/Video01';
import Features03 from 'components/completed/Features03';
import MountTransition from "components/partitial/MountTransition";

import 'react-toastify/dist/ReactToastify.css';
import '../style.scss';
import './style.scss';

toast.configure();

const Kotelnikovo = ({ data }) => {
  const [selectedCompetition, setSelectedCompetition] = useState(null);
  const [globalPadding, setGlobalPadding] = useState(50);

  const handleResize = () => {
    if (!window) return;
    const width = window.innerWidth;
    let padding;
    switch (true) {
      case width <= 400:
        padding = 10;
        break;
      case width > 400 && width <= 600:
        padding = 20;
        break;
      case width > 600 && width <= 800:
        padding = 30;
        break;
      case width > 800 && width <= 1000:
        padding = 40;
        break;
      default:
        padding = 50;
    }

    if (padding !== globalPadding) setGlobalPadding(padding);
  };

  useEffect(() => {
    handleResize();
    window.addEventListener(`resize`, handleResize);
    return () => window.removeEventListener(`resize`, handleResize);
  }, []);

  const formOpen = (value) => {
    setSelectedCompetition(value);
  };
  const formClose = () => {
    setSelectedCompetition(null);
  };

  const competitions = _.get(jsonData, `competitions`);
  const competitionsMenuDropdown = _.map(competitions, (item) => ({
    Component: Link,
    props: {
      to: item.slug,
      spy: true,
      smooth: true,
      duration: 1000,
      offset: -50,
    },
    title: item.title,
  }));
  const competitionsFooterDropdown = _.map(competitions, (item, index) => ({
    title: item.title,
    slug: item.slug,
    func: () => formOpen(index),
  }));

  const isCompetitionSelected = typeof selectedCompetition === `number`;
  const competitionForm = isCompetitionSelected && _.get(jsonData.competitions[selectedCompetition], `form`);
  return (
    <ProjectContext.Provider value={{ projectName: `kotelnikovo`, globalPadding }}>
      <div id="kotelnikovo" className="container">
        <Head>
          <title>{data.siteTitle}</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <Menu02 title={_.get(data, `headerTitle`)} dropdownItems={competitionsMenuDropdown} />
        <Title04
          primaryTitle={_.get(data, `titleTextPrimary`)}
          secondaryTitle={_.get(data, `titleTextSecondary`)}
          imageLogo={_.get(data, `titleImageLogo`)}
          imageBackground={_.get(data, `titleImageBackground`)}
          backgroundPosition="fixed"
        />
        <Features03
          numbersData={_.get(data, `aboutNumbers`)}
          featuresData={_.get(data, `aboutFeatures`)}
          primaryTitle={_.get(data, `aboutPrimaryTitle`)}
          secondaryTitle={_.get(data, `aboutSecondaryTitle`)}
          blockLogo={_.get(data, `aboutBlockLogo`)}
          blockText={_.get(data, `aboutBlockText`)}
        />
        <Element name="photo">
          <Gallery02 data={_.get(data, `photoGalleryItems`)} />
        </Element>
        <Video01 data={_.get(data, `videoGallery`)} />
        <Element name="child">
          <About03
            title={_.get(data, `competitionChildTitle`)}
            about={_.get(data, `competitionChildAbout`)}
            goals={_.get(data, `competitionChildGoals`)}
            persons={_.get(data, `competitionChildPersons`)}
          />
          <Works01 competition="child" />
        </Element>
        <Element name="architect">
          <About03
            title={_.get(data, `competitionArchitectTitle`)}
            about={_.get(data, `competitionArchitectAbout`)}
            goals={_.get(data, `competitionArchitectGoals`)}
            persons={_.get(data, `competitionArchitectPersons`)}
          />
          <Works01 competition="architect" />
        </Element>
        <Element name="contacts">
          <Footer01
            blockLogo={_.get(data, `footerLogo`)}
            blockTitle={_.get(data, `footerTitle`)}
            phoneText={_.get(data, `footerPhoneText`)}
            phoneLink={_.get(data, `footerPhoneLink`)}
            address={_.get(data, `footerAddress`)}
            socialNetworks={_.get(data, `socialNetworks`)}
            dropdownItems={competitionsFooterDropdown}
          />
        </Element>
        <MountTransition mounted={isCompetitionSelected}>
          <Form02 data={competitionForm} onClose={formClose} />
        </MountTransition>
      </div>
    </ProjectContext.Provider>
  );
};

Kotelnikovo.getInitialProps = async () => {
  try {
    const data = parseResponse({
      response: await getSiteData(),
      normalize: normalizeSiteData,
    });
    const photos = parseResponse({
      response: await getPhotos(),
    });
    data.photoGalleryItems = photos;
    return { data };
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return {};
  }
};

export default Kotelnikovo;

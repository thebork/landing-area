import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import cn from 'classnames';
import b_ from 'b_';
import _ from 'lodash';
import { toast } from "react-toastify";

import { parseError, parseResponse, sortSiteData } from "components/helper";
import { getSiteData } from "models/kotelnikovo/api";

import withAuthorization from "components/hocs/withAuthorization";
import SiteInformation from "components/custom/admin/SiteInformation";
import FileLoader from "components/custom/admin/FileLoader";

import 'react-toastify/dist/ReactToastify.css';
import '../../style.scss';
import '../style.scss';
import './style.scss';

toast.configure({
  autoClose: 2000,
});

const b = b_.lock(`AdminPanel`);
const Admin = () => {
  const [selectedSiteItem, setSelectedSiteItem] = useState(null);
  const [selectedLoaderItem, setSelectedLoaderItem] = useState(null);

  const [shouldLoadingData, setShouldLoadingData] = useState(true);
  const [siteInformation, setSiteInformation] = useState(null);

  const selectItem = ({ type, index }) => {
    setSelectedSiteItem(type === `site` ? index : null);
    setSelectedLoaderItem(type === `loader` ? index : null);
  };

  const fetchData = async () => {
    try {
      const response = parseResponse({
        response: await getSiteData(),
        normalize: sortSiteData,
      });
      if (_.size(response)) {
        setSiteInformation(response);
        selectItem({ type: `site`, index: 0 });
      }
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
    setShouldLoadingData(false);
  };

  useEffect(() => {
    if (!shouldLoadingData) return;
    fetchData();
  }, [shouldLoadingData]);

  return (
    <div id="kotelnikovo" className={b()}>
      <Head>
        <title>Панель управления</title>
      </Head>
      <div className={b()}>
        <div className={b(`left`)}>
          <div className={b(`left-title`)}>Настройки сайта</div>
          {
            siteInformation && siteInformation.map((item, index) => (
              <div
                key={item.name}
                className={cn(b(`left-link`), { selected: selectedSiteItem === index })}
                onClick={() => selectItem({ type: `site`, index })}
              >
                {item.name}
              </div>
            ))
          }
          <div className={b(`left-title`)}>Загрузка файлов</div>
          <div
            className={cn(b(`left-link`), { selected: selectedLoaderItem === 0 })}
            onClick={() => selectItem({ type: `loader`, index: 0 })}
          >
            Фотографии
          </div>
          <div
            className={cn(b(`left-link`), { selected: selectedLoaderItem === 1 })}
            onClick={() => selectItem({ type: `loader`, index: 1 })}
          >
            Файлы сайта
          </div>
        </div>
        <div className={b(`right`)}>
          { siteInformation && typeof selectedSiteItem === `number`
            ? <SiteInformation area={_.get(siteInformation[selectedSiteItem], `name`)} />
            : null }
          { typeof selectedLoaderItem === `number`
            ? <FileLoader area={selectedLoaderItem === 0 ? `photos` : `files`} />
            : null }
        </div>
      </div>
    </div>
  );
};

export default withAuthorization(Admin);

import React, { useState } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import b_ from 'b_';
import _ from 'lodash';
import { toast } from 'react-toastify';
import { login } from 'models/kotelnikovo/api';
import { parseError, parseResponse } from "components/helper";
import { setCookie } from "nookies";

import 'react-toastify/dist/ReactToastify.css';
import '../../style.scss';
import '../style.scss';
import './style.scss';
import {
  Box, Button, Grid, TextField,
} from "@material-ui/core";

toast.configure();

const b = b_.lock(`LoginPage`);
const Auth = () => {
  const [username, setUsername] = useState(``);
  const [password, setPassword] = useState(``);

  const loginAction = async () => {
    if (!username || !password) return toast.error(`Не заполнены обязательные поля`);
    try {
      const response = parseResponse({
        response: await login({ username, password }),
      });
      const token = _.get(response, `token`);
      setCookie(null, `token`, token, {
        maxAge: 30 * 24 * 60 * 60,
        path: `/`,
      });
      toast.success(`Авторизация прошла успешно`);
      await Router.push(`/kotelnikovo/admin`);
    } catch (error) {
      const errorMessage = parseError({ error });
      toast.error(errorMessage);
    }
  };

  return (
    <div id="kotelnikovo" className={b()}>
      <Head>
        <title>Страница авторизации</title>
      </Head>
      <main className={b(`content`)}>
        <h1>Пожалуйста, авторизуйтесь</h1>
        <Grid container spacing={3}>
          <Grid item xs={4} />
          <Grid container item xs={4}>
            <Grid item xs={12}>
              <TextField
                id="login"
                label="Имя пользователя"
                variant="outlined"
                size="small"
                margin="dense"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="password"
                label="Пароль"
                variant="outlined"
                size="small"
                margin="dense"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                fullWidth
              />
            </Grid>
            <Grid container item xs={12} justify="center" alignItems="center">
              <Box my={2}>
                <Button variant="contained" color="primary" onClick={loginAction}>Войти</Button>
              </Box>
            </Grid>
          </Grid>
          <Grid item xs={4} />
        </Grid>
      </main>
    </div>
  );
};

export default Auth;

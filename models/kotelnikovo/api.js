import api from 'api';

const projectName = `kotelnikovo`;

export const getWorksCount = ({ competition }) => api.get(`/${projectName}/getWorksCount?competition=${competition}`);
export const getWorksPage = ({ competition, page }) => api.get(
  `/${projectName}/getWorksPage?competition=${competition}&page=${page}`,
);
export const requestCode = ({ email, competition }) => api.post(
  `/${projectName}/vote/setEmail`, {
    email,
    competition,
  },
);

export const sendCode = ({
  email, competition, code, id,
}) => api.post(
  `/${projectName}/vote/sendCode`, {
    email,
    competition,
    code,
    id,
  },
);

export const login = ({
  username, password,
}) => api.post(`/${projectName}/auth/signin`, { username, password });

export const getUser = (token) => api.get(`/${projectName}/auth/getUser`, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export const getSiteData = (block) => api.get(`/${projectName}/info${block ? `/?block=${encodeURI(block)}` : ``}`);
export const getPhotos = () => api.get(`/${projectName}/files/photos/`);
export const getFiles = ({ token }) => api.get(`/${projectName}/files/images`, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export const uploadPhotos = ({ formData, token }) => api({
  method: `post`,
  url: `/${projectName}/files/photos`,
  headers: {
    'Content-Type': `multipart/form-data`,
    Authorization: `Bearer ${token}`,
  },
  data: formData,
});

export const uploadFiles = ({ formData, token }) => api({
  method: `post`,
  url: `/${projectName}/files/images`,
  headers: {
    'Content-Type': `multipart/form-data`,
    Authorization: `Bearer ${token}`,
  },
  data: formData,
});

export const removePhoto = ({ file, token }) => api.delete(`/${projectName}/files/photos?file=${file}`, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export const removeFile = ({ file, token }) => api.delete(`/${projectName}/files/images?file=${file}`, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export const setSiteData = ({ name, value, token }) => api.post(`/${projectName}/info`, { name, value }, {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});
